﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using drContext;
using System.Security.Cryptography;
using Devart.Data.Linq;

namespace Driver_Reports
{
    class UserManager
    {
        private drContext.drDataContext db_ctx; //Database connection
        

        public UserManager(drDataContext databaseContext) 
        {
            db_ctx = databaseContext;
        }
        
        public bool AddDriver(drContext.Driver driver) 
        {
            if (db_ctx.Drivers.Any(p => p.Username == driver.Username))
                return false;
            try
            {
                db_ctx.Drivers.InsertOnSubmit(driver);
                db_ctx.SubmitChanges();
            }

            catch (Exception e) 
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }

            return true;
        }

        public bool AddExec(string username, string pass, uint salt,AccessEnum access) 
        {
            if (db_ctx.Execs.Any(p => p.Username == username))
                return false;
            db_ctx.Execs.InsertOnSubmit(new Exec
            {
                Username = username,
                AccessLevel = (int)access,
                Salt = (int)salt,
                PassHash = this.GenerateHash(salt,pass).GetString()
            });
            db_ctx.SubmitChanges();
            return true;
        }

        public bool DeleteExec(string username) 
        {
            var exec = (from e in db_ctx.Execs
                        where e.Username == username
                        select e).FirstOrDefault();
            if (exec == null)
                return false;
            db_ctx.Execs.DeleteOnSubmit(exec);
            db_ctx.SubmitChanges();
            return true;
        }

        public void DeactivateDriver(int driverID) 
        {
            var user = (from item in db_ctx.GetTable<drContext.Driver>()
                       where item.ID == driverID
                       select item).FirstOrDefault();

            if (user == null)
                return;
            else 
            {
                user.Active = false;
                db_ctx.SubmitChanges();
            }
        }

        public byte[] GenerateHash(uint salt, string pass) 
        {
            var pass_arr = Encoding.UTF8.GetBytes(pass);
            var salted_arr = new byte[pass_arr.Length+4];
            var int_arr = BitConverter.GetBytes(salt);
            salted_arr[0] = int_arr[0];
            salted_arr[1] = int_arr[1];
            salted_arr[2] = int_arr[2];
            salted_arr[3] = int_arr[3];
            Array.Copy(pass_arr, 0, salted_arr, 4, pass_arr.Length);
            Array.Clear(pass_arr, 0, pass_arr.Length);
            var sha = new SHA256Managed();
            var ret_arr = sha.ComputeHash(salted_arr);
            return ret_arr;
        }

        public AccessEnum CheckUser(string username, string pass) 
        {
            int? salted = (from driver in db_ctx.Drivers
                          where driver.Username == username
                          select driver.Salt ).FirstOrDefault();
            if (salted != 0) //test against Drivers table
            {
                var passToCheck = this.GenerateHash((uint)salted, pass).GetString();

                var res = (from driver in db_ctx.Drivers
                           where (driver.Username == username)
                           where (driver.PasswordHash == passToCheck)
                           select driver.Username).ToList();
                if (res.Count == 1)
                    return AccessEnum.Driver;
            }
            else //Test against Execs table
            {
                salted = (from exec in db_ctx.Execs
                          where exec.Username == username
                          select exec.Salt).FirstOrDefault();

                if (salted == null)
                    return AccessEnum.NonExistant;

                var passToCheck = this.GenerateHash((uint)salted, pass).GetString();
                var res = (from exec in db_ctx.Execs
                           where (exec.Username == username)
                           where (exec.PassHash == passToCheck)
                           select exec).ToList();
                if (res.Count == 1)
                    return (AccessEnum)res[0].AccessLevel;
            }
            
            return AccessEnum.NonExistant;
        }

        public Driver GetDriver(string username) 
        {
            return  (from driver in db_ctx.Drivers
                    where driver.Username == username
                    select driver).FirstOrDefault();
        }
    }
}
