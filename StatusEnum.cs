﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Driver_Reports
{
    public static class StatusEnum
    {
        public static long Started { get { return 20; } }
        public static long Stopped { get { return 21; } }
    }

    public enum AccessEnum 
    {
        Admin = 10,
        Exec,
        Driver,
        NonExistant
    }
}
