﻿namespace Driver_Reports
{
    partial class ExecutiveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRate = new System.Windows.Forms.TextBox();
            this.lblRate = new System.Windows.Forms.Label();
            this.lblUnit = new System.Windows.Forms.Label();
            this.lstEmployees = new System.Windows.Forms.ListBox();
            this.lblEmployeeList = new System.Windows.Forms.Label();
            this.lblUpdate = new System.Windows.Forms.Button();
            this.lblMoney = new System.Windows.Forms.Label();
            this.lblEur = new System.Windows.Forms.Label();
            this.lblTasks = new System.Windows.Forms.Label();
            this.lstTasks = new System.Windows.Forms.ListBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(51, 118);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(35, 20);
            this.txtRate.TabIndex = 0;
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Location = new System.Drawing.Point(15, 121);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(30, 13);
            this.lblRate.TabIndex = 1;
            this.lblRate.Text = "Rate";
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.Location = new System.Drawing.Point(92, 121);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(47, 13);
            this.lblUnit.TabIndex = 2;
            this.lblUnit.Text = "EUR / h";
            // 
            // lstEmployees
            // 
            this.lstEmployees.FormattingEnabled = true;
            this.lstEmployees.Location = new System.Drawing.Point(15, 26);
            this.lstEmployees.Name = "lstEmployees";
            this.lstEmployees.Size = new System.Drawing.Size(94, 108);
            this.lstEmployees.TabIndex = 3;
            // 
            // lblEmployeeList
            // 
            this.lblEmployeeList.AutoSize = true;
            this.lblEmployeeList.Location = new System.Drawing.Point(14, 10);
            this.lblEmployeeList.Name = "lblEmployeeList";
            this.lblEmployeeList.Size = new System.Drawing.Size(95, 13);
            this.lblEmployeeList.TabIndex = 4;
            this.lblEmployeeList.Text = "Current Employees";
            // 
            // lblUpdate
            // 
            this.lblUpdate.Location = new System.Drawing.Point(84, 81);
            this.lblUpdate.Name = "lblUpdate";
            this.lblUpdate.Size = new System.Drawing.Size(64, 31);
            this.lblUpdate.TabIndex = 5;
            this.lblUpdate.Text = "Update";
            this.lblUpdate.UseVisualStyleBackColor = true;
            this.lblUpdate.Click += new System.EventHandler(this.lblUpdate_Click);
            // 
            // lblMoney
            // 
            this.lblMoney.AutoSize = true;
            this.lblMoney.Location = new System.Drawing.Point(42, 10);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(62, 13);
            this.lblMoney.TabIndex = 6;
            this.lblMoney.Text = "EUR spent:";
            // 
            // lblEur
            // 
            this.lblEur.AutoSize = true;
            this.lblEur.Location = new System.Drawing.Point(110, 10);
            this.lblEur.Name = "lblEur";
            this.lblEur.Size = new System.Drawing.Size(21, 13);
            this.lblEur.TabIndex = 7;
            this.lblEur.Text = "XX";
            // 
            // lblTasks
            // 
            this.lblTasks.AutoSize = true;
            this.lblTasks.Location = new System.Drawing.Point(68, 165);
            this.lblTasks.Name = "lblTasks";
            this.lblTasks.Size = new System.Drawing.Size(36, 13);
            this.lblTasks.TabIndex = 9;
            this.lblTasks.Text = "Tasks";
            // 
            // lstTasks
            // 
            this.lstTasks.FormattingEnabled = true;
            this.lstTasks.Location = new System.Drawing.Point(24, 181);
            this.lstTasks.Name = "lstTasks";
            this.lstTasks.Size = new System.Drawing.Size(112, 134);
            this.lstTasks.TabIndex = 8;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lblStatus);
            this.splitContainer1.Panel1.Controls.Add(this.lblEmployeeList);
            this.splitContainer1.Panel1.Controls.Add(this.lstEmployees);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lblUpdate);
            this.splitContainer1.Panel2.Controls.Add(this.lblTasks);
            this.splitContainer1.Panel2.Controls.Add(this.lblRate);
            this.splitContainer1.Panel2.Controls.Add(this.lstTasks);
            this.splitContainer1.Panel2.Controls.Add(this.txtRate);
            this.splitContainer1.Panel2.Controls.Add(this.lblEur);
            this.splitContainer1.Panel2.Controls.Add(this.lblUnit);
            this.splitContainer1.Panel2.Controls.Add(this.lblMoney);
            this.splitContainer1.Size = new System.Drawing.Size(403, 315);
            this.splitContainer1.SplitterDistance = 248;
            this.splitContainer1.TabIndex = 10;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(19, 147);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 5;
            // 
            // ExecutiveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 315);
            this.Controls.Add(this.splitContainer1);
            this.Name = "ExecutiveForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ExecutiveForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.Label lblRate;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.ListBox lstEmployees;
        private System.Windows.Forms.Label lblEmployeeList;
        private System.Windows.Forms.Button lblUpdate;
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.Label lblEur;
        private System.Windows.Forms.Label lblTasks;
        private System.Windows.Forms.ListBox lstTasks;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblStatus;

    }
}