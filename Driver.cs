﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Diagnostics;
using drContext;

namespace Driver_Reports
{
    public partial class DriverForm : Form
    {
        //form private vars
        private Timer tt;
        private drDataContext db;
        private Driver driver;
        private Task currentTask;
        private TimeSpan workedTime;
        private TimeSpan second;
        
        public DriverForm(Driver LoggedInDriver, drDataContext database)
        {
            InitializeComponent();
            tt = new Timer();
            tt.Interval = 1000;
            tt.Tick += (sender, e) => { this.UpdateTimeLabel(); };
            tt.Stop();
            db = database;
            driver = LoggedInDriver;
            workedTime = new TimeSpan(0);
            UpdateTimeLabel();
            second = new TimeSpan(0, 0, 0, 1);
            SetUpContext();
            lblWorked.Text = this.TimeFormat(GetTodayWorkedTime());
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        private void UpdateTimeLabel() 
        {
            workedTime = workedTime.Add(second);
            lblWorked.Text = this.TimeFormat(workedTime);
        }

        private string TimeFormat(TimeSpan ts) 
        {
            return String.Format("{0}:{1}:{2}",ts.Hours,ts.Minutes,ts.Seconds);
        }

        private void btnChangeTask_Click(object sender, EventArgs e)
        {
            var lastTask = (from task in db.Histories
                            where task.Driver == driver
                            orderby task.RecordTime descending
                            select task).FirstOrDefault();
            if (lastTask == null) 
            {
              
            }
            else if (lastTask.WorkStatus == StatusEnum.Started)
            {
                MessageBox.Show("Please end the current task!","Task Progress");
                return;
            }
                 
            using (var g = new TaskList(db))
            {
                var res = g.ShowDialog();
                switch (res)
                {
                    case System.Windows.Forms.DialogResult.OK:
                        {
                            currentTask = g.SelectedTask;
                            lblRate.Text = currentTask.HourlyRate.ToString();
                            lblTask.Text = currentTask.Name;
                        }
                        break;
                    default:
                        break;
                }
            }

            
        }

        private void btnStart_Click(object sender, EventArgs e)
        {

            if (currentTask == null)
            {
                MessageBox.Show("Please select a task first!");
                return;
            }

            var lastTask = (from task in db.Histories
                            where task.Driver == driver
                            orderby task.RecordTime descending
                            select task).FirstOrDefault();
            if (lastTask == null)
            {

            }
            else if (lastTask.WorkStatus == StatusEnum.Started)
            {
                MessageBox.Show("Please end the current task!", "Task Progress");
                return;
            }

            this.InsertCurrentTaskStatus(StatusEnum.Started, null);
            tt.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            var lastTask = (from task in db.Histories
                            where task.Driver == driver
                            orderby task.RecordTime descending
                            select task).FirstOrDefault();
            if (lastTask == null)
            {

            }
            else if (lastTask.WorkStatus == StatusEnum.Started)
            {
                currentTask = lastTask.Task;
                this.InsertCurrentTaskStatus(StatusEnum.Stopped, DateTime.Now.Subtract(lastTask.RecordTime).Seconds);
            }

            tt.Stop();
            lblRate.Text = "00";
            lblTask.Text = "NONE";
            currentTask = null;
        }

        private void InsertCurrentTaskStatus(long status, long? workedTime) 
        {
            if (currentTask == null)
            {
                Debug.WriteLine("currentTask is null");
                return;
            }
            var record = new History();
            record.Driver = driver;
            record.RecordTime = DateTime.Now;
            record.Task = currentTask;
            record.WorkedTime = workedTime;
            record.WorkStatus = status;
            db.Histories.InsertOnSubmit(record);
            db.SubmitChanges();
        }

        private void GetTodayStartTime() 
        {
            var lastTask = (from task in db.Histories
                        where task.Driver == driver
                        where task.RecordTime.Year == DateTime.Now.Year
                        where task.RecordTime.DayOfYear == DateTime.Now.DayOfYear
                        orderby task.RecordTime ascending
                        select task).FirstOrDefault();

            if (lastTask == null)
            {
                workedTime = new TimeSpan(0);
            }
            else 
            {
                workedTime = DateTime.Now.Subtract(lastTask.RecordTime);
            }
        }

        private void SetUpContext() 
        {
            double started = 0;
            var lastTask = (from task in db.Histories
                            where task.Driver == driver
                            orderby task.RecordTime descending
                            select task).FirstOrDefault();
            if (lastTask == null)
            {

            }
            else if (lastTask.WorkStatus == StatusEnum.Started)
            {
                currentTask = lastTask.Task;
                lblRate.Text = currentTask.HourlyRate.ToString();
                lblTask.Text = currentTask.Name;
                started = DateTime.Now.Subtract(lastTask.RecordTime).Seconds;
                tt.Start();
            }

            var todayTasks = (from task in db.Histories
                              where task.Driver == driver
                              where task.RecordTime.Year == DateTime.Now.Year
                              where task.RecordTime.DayOfYear == DateTime.Now.DayOfYear
                              where task.WorkStatus == StatusEnum.Stopped
                              orderby task.RecordTime ascending
                              select task);

            double hours = started/3600D;
            foreach (var task in todayTasks)
            {
                hours += Math.Round((double)(task.WorkedTime / 3600D), 3);
            }
            workedTime = TimeSpan.FromHours(hours);

        }

        private void ProgressAlert()
        {
            var todayTasks = (from task in db.Histories
                            where task.Driver == driver
                            where task.RecordTime.Year == DateTime.Now.Year
                            where task.RecordTime.DayOfYear == DateTime.Now.DayOfYear
                            where task.WorkStatus == StatusEnum.Stopped
                            orderby task.RecordTime ascending
                            select task);

            var sb = new StringBuilder();
            sb.Append("Today's done work:\n");
            foreach (var task in todayTasks) 
            {
                sb.Append(task.Task.Name+": "+Math.Round((double)(task.WorkedTime/3600D),3) +" hours\n");
            }

            MessageBox.Show(String.Format("{0:D3}",sb.ToString()), "Progress");

        }

        private void btnProgress_Click(object sender, EventArgs e)
        {
            ProgressAlert();
        }

        private TimeSpan GetTodayWorkedTime() 
        {
            var todayTasks = (from task in db.Histories
                            where task.Driver == driver
                            where task.RecordTime.Year == DateTime.Now.Year
                            where task.RecordTime.DayOfYear == DateTime.Now.DayOfYear
                            where task.WorkStatus == StatusEnum.Stopped
                            orderby task.RecordTime ascending
                            select task);

            double hours = 0;
            foreach (var task in todayTasks) 
            {
                hours += Math.Round((double)(task.WorkedTime/3600D),3);
            }

            return TimeSpan.FromHours(hours);
        }

    }
}
