﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Reference_Tables
    {

        [Table("Execs")]
        public class SuperiorsEntity
        {
            public uint ID { get; set; }
            public string Username { get; set; }
            public string Salt { get; set; }
            public string PasswordHash { get; set; }
            public AccessEnum AccessLevel { get; set; } //integer
        }
    }
