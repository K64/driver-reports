﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Reference_Tables
    {

        [Table("Drivers")]
        public class DriverEntity
        {
            public uint ID { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string Profession { get; set; }
            public uint Phone { get; set; }
            public string Address { get; set; }
            public string PasswordHash { get; set; }
            public string Salt { get; set; }
            public string Username { get; set; }
            public bool Active { get; set; }
        }

    }
