﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reference_Tables
    {

        public class History
        {
            public uint ID { get; set; }
            public uint DriverID { get; set; } //foreign key to Driver table record
            public uint TaskID { get; set; } //foreign key to Task table record
            public StatusEnum WorkStatus { get; set; } //integer (StatusEnum is just an int) indicating has work started or stopped
            public DateTime RecordTime { get; set; } //DATETIME type in SQL
            public int? WorkedTime { get; set; } // integer, can be null(this column can be empty), showing hours worked for the last task

        }
    }
