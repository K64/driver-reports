﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using drContext;

namespace Driver_Reports
{
    public partial class Add_Employee : Form
    {
        private drContext.drDataContext db;

        public Add_Employee(drDataContext database)
        {
            InitializeComponent();
            db = database;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtAddress.Text.Length == 0 |
               txtName.Text.Length == 0 |
               txtPhone.Text.Length == 0 |
               txtSurname.Text.Length == 0 |
               txtProfession.Text.Length == 0 |
               txtUsername.Text.Length == 0 |
               txtPass.Text.Length == 0
                ) 
            {
                MessageBox.Show("All fields must be filled!", "Alert");
                return;
            }

            var exists = (from driver in db.Drivers
                          where driver.Username == txtUsername.Text
                          select driver).Any();

            if (exists) 
            {
                MessageBox.Show("Such username already exists!", "Alert");
                return;
            }

            var um = new UserManager(db);
            var drivernew = new Driver();
            drivernew.Active = true;
            drivernew.Address = txtAddress.Text;
            drivernew.Name = txtName.Text;
            drivernew.Salt = new Random().Next(Int32.MaxValue);
            drivernew.PasswordHash = um.GenerateHash((uint)drivernew.Salt, txtPass.Text).GetString();
            drivernew.Phone = txtPhone.Text;
            drivernew.Profession = txtProfession.Text;
            drivernew.Surname = txtSurname.Text;
            drivernew.Username = txtUsername.Text;

            if (um.AddDriver(drivernew))
            {
                MessageBox.Show("User: " + txtUsername.Text + " added!");
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else 
            {
                MessageBox.Show("Error working with database!","Alert");
            }

        }

    }
}
