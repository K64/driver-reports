﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using drContext;

namespace Driver_Reports
{
    public partial class TaskList : Form
    {
        private drDataContext db;
        public TaskList(drDataContext database)
        {
            InitializeComponent();
            btnChange.Click += (sender, e) => { this.btnChange_Click(); };
            db = database;
            PopulateList();
        }

        private void btnChange_Click() 
        {
            this.SelectedTask = (drContext.Task)lstTasks.SelectedItem;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        public void PopulateList()
        {
            lstTasks.DisplayMember = "Name";

            var items = from task in db.Tasks
                        select task;
            lstTasks.Items.AddRange(items.ToArray());
        }

        public drContext.Task SelectedTask { get; set; }

    }
}
