﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace Driver_Reports
{
    public class Preferences
    {
        private Variables vars;


        public Preferences()
        {
            vars = new Variables();
        }

        /// <summary>
        /// Settings are stored here. Can be written to.
        /// </summary>
        public Variables Vars
        {
            get { return vars; }
            set { vars = value; }
        }



        public void StoreVars()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Variables));
            StreamWriter w = new StreamWriter("vars.xml");
            serializer.Serialize(w, vars);
            w.Close();
        }

        public void LoadUp()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Variables));
            StreamReader r = new StreamReader("vars.xml");
            vars = (Variables)serializer.Deserialize(r);
            r.Close();
        }

        [Serializable]
        public class Variables
        {
            public string DatabaseURL { get; set; }
            public string DatabaseUser { get; set; }
            public string DatabasePass { get; set; }
            public string Database { get; set; }
        }
    }

}
