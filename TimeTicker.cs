﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Driver_Reports
{
    public class TimeTicker
    {
        private Timer timer;
        public string TimeFormat = "HH:mm:ss";

        private string theTime;
        public string TheTime
        {
            get
            {
                return theTime;
            }
            set
            {
                theTime = value;
                OnTheTimeChanged(this.theTime);
            }
        }

        public TimeTicker()
        {
            timer = new Timer();
            timer.Tick += new EventHandler(Timer_Tick);
            timer.Interval = 1000;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            TheTime = DateTime.Now.ToString(TimeFormat);
        }

        public delegate void TimerTickHandler(string newTime);
        public event TimerTickHandler TheTimeChanged;

        protected void OnTheTimeChanged(string newTime)
        {
            if (TheTimeChanged != null)
            {
                TheTimeChanged(newTime);
            }
        }
    }
}
