﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Driver_Reports
{
    public static class ExtensionUtilities
    {
        public static String GetString(this byte[] value) 
        {
            return Encoding.UTF8.GetString(value);
        }

    }
}
