# README #

### What is this repository for? ###

Driver Reports app for Sys. Analysis and Development course. Task is to implement and document a system which records what the drivers are doing currently, how much are they getting paid and provide few statistics to the overseeing body, like boss. Also required is an admin panel, for managing user accounts.

### General app info ###

Runs on .NET 4.5, written in C#. If you don't have it, get it at https://www.microsoft.com/en-us/download/details.aspx?id=30653

Database: MySQL.


### WIKI ###

All development and document status info must be shared here in the [WIKI](https://bitbucket.org/K64/driver-reports/wiki/)

### Who do I talk to? ###

If there any questions about what to do, state of the documents (SRS etc.), ask in skype group chat.