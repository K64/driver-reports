﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using drContext;

namespace Driver_Reports
{
    public partial class AdminForm : Form
    {
        private drDataContext db;
        private UserManager um;

        public AdminForm(drDataContext database)
        {
            InitializeComponent();
            db = database;
            PopulateList();
            um = new UserManager(db);
            lstEmployees.SelectionMode = SelectionMode.MultiExtended;
        }

        protected override void OnClosed(EventArgs e)
        {
       //     Application.Exit();
            base.OnClosed(e);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var add = new Add_Employee(db);
            add.ShowDialog();
            if (add.DialogResult == System.Windows.Forms.DialogResult.OK)
                PopulateList();
        }

        private void PopulateList() 
        {
            lstEmployees.DisplayMember = "FullName";

            var actives = from driver in db.Drivers
                          where driver.Active == true
                          select driver;
            lstEmployees.Items.Clear();
            lstEmployees.Items.AddRange(actives.ToArray());
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (var item in lstEmployees.SelectedItems) 
            {
                um.DeactivateDriver(((Driver)item).ID);
            }

            PopulateList();
        }
    }
}
