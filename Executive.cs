﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using drContext;

namespace Driver_Reports
{
    public partial class ExecutiveForm : Form
    {
        private drDataContext db;

        public ExecutiveForm(drDataContext database)
        {
            InitializeComponent();
            db = database;
            lstEmployees.SelectionMode = SelectionMode.One;
            lstTasks.SelectionMode = SelectionMode.One;
            lstTasks.SelectedValueChanged += UpdateRateField;
            lstEmployees.SelectedValueChanged += UpdateStatusLabel;
            PopulateEmployeeList();
            PopulateTaskList();
            lblEur.Text = GetCost().ToString();
        }

        private void UpdateRateField(object sender, EventArgs e)
        {
            txtRate.Text = ((Task)lstTasks.SelectedItem).HourlyRate.ToString();
        }

        private void UpdateStatusLabel(object sender, EventArgs e)
        {
            lblStatus.Text = this.TimeFormat(GetTodayWorkedTime());
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        private void PopulateEmployeeList()
        {
            lstEmployees.DisplayMember = "FullName";

            var actives = from driver in db.Drivers
                          where driver.Active == true
                          select driver;
            lstEmployees.Items.Clear();
            lstEmployees.Items.AddRange(actives.ToArray());
        }

        private void PopulateTaskList()
        {
            lstTasks.DisplayMember = "Name";

            var tasks = from task in db.Tasks
                          select task;
            lstTasks.Items.Clear();
            lstTasks.Items.AddRange(tasks.ToArray());
        }

        private void UpdateTaskRate() 
        {
            ((Task)lstTasks.SelectedItem).HourlyRate = int.Parse(txtRate.Text);
            db.SubmitChanges();
        }

        private void lblUpdate_Click(object sender, EventArgs e)
        {
            UpdateTaskRate();
        }

        private TimeSpan GetTodayWorkedTime()
        {
            var dr = ((Driver)lstEmployees.SelectedItem);
            var todayTasks = (from task in db.Histories
                              where task.Driver.ID == dr.ID-1
                              where task.RecordTime.Year == DateTime.Now.Year
                              where task.RecordTime.DayOfYear == DateTime.Now.DayOfYear
                              where task.WorkStatus == StatusEnum.Stopped
                              orderby task.RecordTime ascending
                              select task);

            double hours = 0;
            foreach (var task in todayTasks)
            {
                hours += Math.Round((double)(task.WorkedTime / 3600D), 5);
            }

            return TimeSpan.FromHours(hours);
        }

        private string TimeFormat(TimeSpan ts)
        {
            return String.Format("{0}:{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);
        }

        private double GetCost() 
        {
            double expense = 0;


            foreach (var driver in from items in db.Drivers where items.Active == true select items) 
            {
                var todayTasks = (from task in db.Histories
                                  where task.Driver.ID == driver.ID - 1
                                  where task.RecordTime.Year == DateTime.Now.Year
                                  where task.RecordTime.DayOfYear == DateTime.Now.DayOfYear
                                  where task.WorkStatus == StatusEnum.Stopped
                                  orderby task.RecordTime ascending
                                  select task);

                foreach (var task in todayTasks)
                {
                    expense += Math.Round((double)(task.WorkedTime / 3600D) * task.Task.HourlyRate, 2);
                }
            }

            return expense;

        }

    }
}
