﻿namespace Driver_Reports
{
    partial class DriverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTime = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.lblRateText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblWorked = new System.Windows.Forms.Label();
            this.btnChangeTask = new System.Windows.Forms.Button();
            this.lblTaskText = new System.Windows.Forms.Label();
            this.lblTask = new System.Windows.Forms.Label();
            this.lblPhoneText = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblEurText = new System.Windows.Forms.Label();
            this.lblRate = new System.Windows.Forms.Label();
            this.btnProgress = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblTime.Location = new System.Drawing.Point(162, 9);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(147, 16);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "dd/mm/yyyy HH:mm:ss";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 9);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(70, 33);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start Work";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(12, 48);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(70, 33);
            this.btnStop.TabIndex = 2;
            this.btnStop.Text = "Stop Work";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // lblRateText
            // 
            this.lblRateText.AutoSize = true;
            this.lblRateText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblRateText.Location = new System.Drawing.Point(112, 56);
            this.lblRateText.Name = "lblRateText";
            this.lblRateText.Size = new System.Drawing.Size(40, 16);
            this.lblRateText.TabIndex = 3;
            this.lblRateText.Text = "Rate:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(110, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Time worked:";
            // 
            // lblWorked
            // 
            this.lblWorked.AutoSize = true;
            this.lblWorked.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblWorked.Location = new System.Drawing.Point(196, 114);
            this.lblWorked.Name = "lblWorked";
            this.lblWorked.Size = new System.Drawing.Size(106, 24);
            this.lblWorked.TabIndex = 5;
            this.lblWorked.Text = "HH:mm:ss";
            // 
            // btnChangeTask
            // 
            this.btnChangeTask.Location = new System.Drawing.Point(12, 97);
            this.btnChangeTask.Name = "btnChangeTask";
            this.btnChangeTask.Size = new System.Drawing.Size(70, 39);
            this.btnChangeTask.TabIndex = 6;
            this.btnChangeTask.Text = "Change Task";
            this.btnChangeTask.UseVisualStyleBackColor = true;
            this.btnChangeTask.Click += new System.EventHandler(this.btnChangeTask_Click);
            // 
            // lblTaskText
            // 
            this.lblTaskText.AutoSize = true;
            this.lblTaskText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblTaskText.Location = new System.Drawing.Point(112, 87);
            this.lblTaskText.Name = "lblTaskText";
            this.lblTaskText.Size = new System.Drawing.Size(87, 16);
            this.lblTaskText.TabIndex = 7;
            this.lblTaskText.Text = "Current Task:";
            // 
            // lblTask
            // 
            this.lblTask.AutoSize = true;
            this.lblTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblTask.Location = new System.Drawing.Point(205, 87);
            this.lblTask.Name = "lblTask";
            this.lblTask.Size = new System.Drawing.Size(47, 16);
            this.lblTask.TabIndex = 8;
            this.lblTask.Text = "NONE";
            // 
            // lblPhoneText
            // 
            this.lblPhoneText.AutoSize = true;
            this.lblPhoneText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblPhoneText.Location = new System.Drawing.Point(183, 203);
            this.lblPhoneText.Name = "lblPhoneText";
            this.lblPhoneText.Size = new System.Drawing.Size(56, 16);
            this.lblPhoneText.TabIndex = 9;
            this.lblPhoneText.Text = "Contact:";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblPhone.Location = new System.Drawing.Point(245, 203);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(64, 16);
            this.lblPhone.TabIndex = 10;
            this.lblPhone.Text = "00000000";
            // 
            // lblEurText
            // 
            this.lblEurText.AutoSize = true;
            this.lblEurText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblEurText.Location = new System.Drawing.Point(181, 57);
            this.lblEurText.Name = "lblEurText";
            this.lblEurText.Size = new System.Drawing.Size(39, 16);
            this.lblEurText.TabIndex = 11;
            this.lblEurText.Text = "Eur/h";
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblRate.Location = new System.Drawing.Point(153, 57);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(22, 16);
            this.lblRate.TabIndex = 12;
            this.lblRate.Text = "00";
            // 
            // btnProgress
            // 
            this.btnProgress.Location = new System.Drawing.Point(14, 151);
            this.btnProgress.Name = "btnProgress";
            this.btnProgress.Size = new System.Drawing.Size(68, 30);
            this.btnProgress.TabIndex = 13;
            this.btnProgress.Text = "Progress";
            this.btnProgress.UseVisualStyleBackColor = true;
            this.btnProgress.Click += new System.EventHandler(this.btnProgress_Click);
            // 
            // DriverForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 228);
            this.Controls.Add(this.btnProgress);
            this.Controls.Add(this.lblRate);
            this.Controls.Add(this.lblEurText);
            this.Controls.Add(this.lblPhone);
            this.Controls.Add(this.lblPhoneText);
            this.Controls.Add(this.lblTask);
            this.Controls.Add(this.lblTaskText);
            this.Controls.Add(this.btnChangeTask);
            this.Controls.Add(this.lblWorked);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblRateText);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lblTime);
            this.Name = "DriverForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DriverForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label lblRateText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblWorked;
        private System.Windows.Forms.Button btnChangeTask;
        private System.Windows.Forms.Label lblTaskText;
        private System.Windows.Forms.Label lblTask;
        private System.Windows.Forms.Label lblPhoneText;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblEurText;
        private System.Windows.Forms.Label lblRate;
        private System.Windows.Forms.Button btnProgress;
    }
}