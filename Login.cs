﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using drContext;

namespace Driver_Reports
{
    public partial class Login : Form
    {
        //dummy driver -> username:user password:pass
        //dummy admin -> username:admin password:pass
        //dummy exec -> username:exec password:pass

        drDataContext dr;
        UserManager man;
        Preferences p;

        public Login()
        {
            InitializeComponent();
            btnLogin.Click += (sender, e) => { this.LogIn(); };
            SetUpVars();
        }

        private void SetUpVars() 
        {
            p = new Preferences();
            p.LoadUp();
            string connectionString = "server=" + p.Vars.DatabaseURL + ";uid=" + p.Vars.DatabaseUser + ";pwd=" + p.Vars.DatabasePass + ";database=" + p.Vars.Database + ";charset=utf8mb4";
            dr = new drDataContext(connectionString);
            man = new UserManager(dr);
        }

        protected override void OnClosed(EventArgs e)
        {
            Application.Exit();
            base.OnClosed(e);
        }

        private void LogIn() 
        {
            switch (man.CheckUser(txtUser.Text, txtPass.Text)) 
            {
                case AccessEnum.Driver:
                    new DriverForm(man.GetDriver(txtUser.Text), dr).Show();
                    break;
                case AccessEnum.Admin:
                    new AdminForm(dr).Show();
                    break;
                case AccessEnum.Exec:
                    new ExecutiveForm(dr).Show();
                    break;
                case AccessEnum.NonExistant:
                default:
                    MessageBox.Show("Incorrect. Try again.");
                    break;
            }
        }

    }
}
